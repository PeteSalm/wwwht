var express = require("express");
var app = express();
var server = require("http").createServer(app);
var io = require("socket.io").listen(server);

var users = [];
var connections = [];
var gamearray = ["gamearray"];

/* CREATE DATABASE */
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('mydb.db');
var check;
db.serialize(function() {
  db.run("CREATE TABLE if not exists user_info (username TEXT, password TEXT, gamename TEXT, level REAL, xp REAL, ptsAgi REAL, ptsAcc REAL, ptsJump REAL, ptsPnt REAL)");
  db.run("CREATE TABLE if not exists highscores (username TEXT, score REAL)");
});
db.close();

/* SET SERVER LOCATION */
app.set("host", process.env.IP || "127.0.0.1");
server.listen(process.env.PORT || 3000, app.get("host"));
console.log("IP is " + app.get("host"));

/* GET DIRECTORY */
app.get("/", function(req, res){
	res.sendFile(__dirname+"/ht.html");
});

app.use(express.static(__dirname+"/static"));


io.sockets.on("connection", function(socket) {
	/* WHEN CONNECTION */
	connections.push(socket);
	console.log("Connected: %s sockets connected", connections.length);
	
	
	/* DISCONNECT */
	socket.on("disconnect", function(data){
		
		gamearray = ["gamearray"];
		for(i = 0; i < users.length; i = i + 3) {
				if (users[i] !== socket.id) {
					var tempgn = users[i]+2;
					if (tempgn.length < 8) {
						gamearray[gamearray.length] = tempgn;
					}
				}
			}
		connections.splice(connections.indexOf(socket), 1);
		console.log("Disconnected: %s sockets connected", connections.length);
	});
	
	/* RECEIVING AND SENDING DATA */
	socket.on("send message", function(data) {
		if (data[0] === "gamename") {
			if (!(gamearray.includes(data[1]))) {
				gamearray[gamearray.length] = data[2];
			}
			var user = data[1];
			var gname = data[2];

			var sqlite3 = require('sqlite3').verbose();
			var db = new sqlite3.Database('./mydb.db');

			db.serialize(function() {
				db.run("UPDATE user_info SET gamename = '"+gname+"' WHERE username = '"+user+"';");
				
			});
			
			users.push(socket.id);
			users.push(user);
			users.push(gname || "");
			
			db.close();
			
		} else if (data[0] === "gamenamerequest") {
			io.emit("new message", {msg: gamearray});
			
		} else if(data[0] === "userinfo") {
			
			var sqlite3 = require('sqlite3').verbose();
			var db = new sqlite3.Database('./mydb.db');

			db.serialize(function() {
				db.all("SELECT username,password FROM user_info", function(err, rows) {
					io.emit("rows", {msg: rows});
				});
			});
			
			db.close();
			
		} else if (data[0] === "newuser") {
			var user = data[1];
			var pass = data[2];
			var sqlite3 = require('sqlite3').verbose();
			var db = new sqlite3.Database('./mydb.db');
			db.serialize(function() {
				var stmt = db.prepare("INSERT INTO user_info VALUES (?,?,?,?,?,?,?,?, ?)")
				stmt.run(user, pass, "", "0", "0", "0", "0", "0", "0");
				stmt.finalize();
				console.log("New user created!");
			});
			
			db.close();
			
		} else if (data[0] === "getuserdata") {
			var user = data[1];
			var sqlite3 = require('sqlite3').verbose();
			var db = new sqlite3.Database('./mydb.db');

			db.serialize(function() {
				db.all('SELECT * FROM user_info WHERE username = "'+user+'"', function(err, rows) {

					io.emit("userdata", {msg: rows});
					
				});
			});
			
			db.close();
			
		} else if (data[0] === "connectroom") {
			io.emit("connectroom", {msg: data});
			gamearray = ["gamearray"];
			for(i = 0; i < users.length; i = i + 3) {
				if (users[i] !== socket.id) {
					var tempgn = users[i]+2;
					if (tempgn.length < 8) {
						gamearray[gamearray.length] = tempgn;
					}
				}
			}
			
		} else if (data[0] === "deleteroom") {
			gamearray = ["gamearray"];

			for(i = 0; i < users.length; i = i + 3) {
				if (users[i] !== socket.id) {
					var tempgn = users[i]+2;
					if (tempgn.length < 8) {
						gamearray[gamearray.length] = tempgn;
					}
				}
			}
		
		} else if (data[0] === "updatexp") {
			
			var xp = data[1];
			var user = data[2];
			var score = data[3];
			
			var sqlite3 = require('sqlite3').verbose();
			var db = new sqlite3.Database('./mydb.db');

			db.serialize(function() {
				db.run("UPDATE user_info SET xp = '"+xp+"' WHERE username = '"+user+"';");

				var stmt = db.prepare("INSERT INTO highscores VALUES (?,?)")
				stmt.run(user, score);
				stmt.finalize();
			});
			
		} else if (data[0] === "updatelevel") {
			
			var level = data[1];
			var points = data[2];
			var user = data[3];
			
			var sqlite3 = require('sqlite3').verbose();
			var db = new sqlite3.Database('./mydb.db');

			db.serialize(function() {
				db.run("UPDATE user_info SET level = '"+level+"' WHERE username = '"+user+"';");
				db.run("UPDATE user_info SET ptsPnt = '"+points+"' WHERE username = '"+user+"';");
				
			});
			
		} else if (data[0] === "updatepoints") {
			var agi = data[1];
			var acc = data[2];
			var jump = data[3];
			var points = data[4];
			var user = data[5];
			
			var sqlite3 = require('sqlite3').verbose();
			var db = new sqlite3.Database('./mydb.db');

			db.serialize(function() {
				db.run("UPDATE user_info SET ptsAgi = '"+agi+"' WHERE username = '"+user+"';");
				db.run("UPDATE user_info SET ptsJump = '"+jump+"' WHERE username = '"+user+"';");
				db.run("UPDATE user_info SET ptsAcc = '"+acc+"' WHERE username = '"+user+"';");
				db.run("UPDATE user_info SET ptsPnt = '"+points+"' WHERE username = '"+user+"';");
			});
			
		} else if (data[0] === "gethighscores") {
			var sqlite3 = require('sqlite3').verbose();
			var db = new sqlite3.Database('./mydb.db');

			db.serialize(function() {
				db.all('SELECT * FROM highscores ', function(err, rows) {

					io.emit("highscores", {msg: rows});
					
				});
			});
			
			db.close();
			
			
		} else {
			io.emit("new message", {msg: data});
		}
	});
});